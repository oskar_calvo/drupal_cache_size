<?php

/**
 * @file routing file
 */

/**
 * implement hook_menu().
 */
function cache_size_delete_menu(){
  $file_path = drupal_get_path('module', 'cache_size_delete'). '/src/Form';
  $item['admin/config/development/cache_size/delete-config-form'] = [
    'title' => 'Cache table delete',
    'description' => 'Configuration form to delete cache tables from database',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['cache_size_delete_configuration'],
    'access arguments' => ['administer cache size'],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'DeleteForm.php',
    'file path' => drupal_get_path('module', 'cache_size_delete'). '/src/Form/',
  ];
  return $item;
}