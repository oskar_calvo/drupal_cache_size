<?php

/**
 * @file Delete configuration form
 */

/**
 * configuration form
 */
function cache_size_delete_configuration($form, &$form_state){
  
  $form['cache_size_delete_enable_cron'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable this option'),
    '#default_value' => variable_get('cache_size_delete_enable_cron'),
  ];
  $form['cache_size_delete_table_size'] = [
    '#type' => 'textfield',
    '#title' => t('size'),
    '#description' => t('Add the size limit for cache tables.'),
    '#default_value' => variable_get('cache_size_delete_table_size'),
    '#size' => 20,
    '#maxlength' => 128,
    '#required' => TRUE,
  ];
  $form['#validate'][] = 'cache_size_delete_form_validate_size';
  
  return system_settings_form($form);
}

/**
 * function to validate the input cache_size_delete_table_size
 *
 * @param $form
 * @param $form_state
 */
function cache_size_delete_form_validate_size($form, $form_state){
  if(!empty($form_state['values']['cache_size_delete_table_size']) && !is_numeric($form_state['values']['cache_size_delete_table_size'])){
      form_set_error('cache_size_delete_table_size', 'The field must be a number.');
    }
}
