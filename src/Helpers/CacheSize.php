<?php

namespace Drupal\cache_size\Helpers;


class CacheSize {
  
  /**
   * @return array|null
   */
  public static function getList(){
    if (!self::getDatabaseName()) {
      return NULL;
    }
  
    $query = self::getTableSize();
    $table_list = array ();
  
    foreach ($query as $key => $table) {
      if (preg_match("/cache/", $table->table, $coincidencias, PREG_OFFSET_CAPTURE)) {
        $table_list[$table->table] =  $table->size ;
      }
    }
    return $table_list;
  }

  /**
   * @name _cache_size_get_table_list
   * @return an array with the list of the cache tables.
   */
  public static function getTableList() {
    
    
    if (!self::getDatabaseName()) {
      return NULL;
    }
    
    $query = self::getTableSize();
    $table_list = array ();
    
    foreach ($query as $key => $table) {
      if (preg_match("/cache/", $table->table, $coincidencias, PREG_OFFSET_CAPTURE)) {
        $table_list[$key]['name'] = $table->table;
        $table_list[$key]['size'] = $table->size . ' Size in MB';
      }
    }
    
    try {
      
      if (empty($table_list)) {
        throw new Exception("There are not cache tables.");
        return NULL;
      }
      else {
        return $table_list;
      }
      
    } catch (Exception $e) {
      watchdog('cache_size', 'Message: !message', array ('!message' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
  
  /**
   * @return \DatabaseStatementInterface
   */
  private static function getTableSize() {
    return db_query('SELECT
                              table_name AS "table", round(((data_length + index_length)/1024/1024),2)"size"
                            FROM
                              information_schema.TABLES
                            WHERE
                              table_schema =:databasename
                            ORDER BY
                              (data_length + index_length)DESC;',
                    array (':databasename' => self::getDatabaseName()));
  }
  
  /**
   * @name cache_size_get_database_name
   * @return string with the name of the database or null if there is nothing
   */
  public static function getDatabaseName() {
    global $databases;
    
    $database_name = $databases['default']['default']['database'];
    
    try {
      if (!isset($database_name)) {
        throw new Exception("I don't find the name of the database");
        return NULL;
      }
      else {
        return $database_name;
      }
    } catch (Exception $e) {
      watchdog('cache_size', 'Message: !message', array ('!message' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
  
}