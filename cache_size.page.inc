<?php

module_load_include('inc', 'cache_size', 'cache_size');
use \Drupal\cache_size\Helpers\CacheSize;

/**
 * @file page include fiel to print the page.
 */

/**
 * @name cache_size_size_view
 * @return print the output of the information in a form.
 */
function cache_size_size_view(){

	$cache_table_list = CacheSize::getTableList();

	if(!$cache_table_list){
 
		return t('there is not cache table'); 
	}

	$header = array(
	 array('data' => t('Name of the table'), 'sort' => 'asc'),
	 array('data' => t('Size of the table in MB')),
	);

  foreach ($cache_table_list as $key => $table) {
    $rows[$key][]['data'] = $table['name'];
    $rows[$key][]['data'] = $table['size'];     
  }

  // http://drupal.stackexchange.com/questions/103171/alternative-ways-to-clear-the-drupal-cache

  return  theme('table',array('header'=>$header,'rows'=>$rows));
}
